{
    "$schema": "http://json-schema.org/draft-07/schema",

    "type": "object",
    "required": ["services", "lastModified"],
    "properties": {
        "services": {
            "type": "array",
            "items": {
                "$ref": "#/definitions/service-type"
            },
            "description": "List of available service types"
        },
        "lastModified": {
            "type": "string",
            "format": "date-time",
            "description": "Timestamp of last modification date for this index"
        }
    },

    "definitions": {
        "service-type": {
            "type": "object",
            "required": ["type", "versions"],
            "properties": {
                "type": {
                    "type": "string",
                    "pattern": "^([a-z]|[a-z][a-z0-9-]*[a-z0-9])$",
                    "maxLength": 63,
                    "description": "The unique name for the service type"
                },
                "versions": {
                    "type": "array",
                    "items": {
                        "$ref": "#/definitions/service-type-version"
                    },
                    "minItems": 1,
                    "description": "The available versions of the service type"
                }
            }
        },
        "service-type-version": {
            "type": "object",
            "allOf": [
                {
                    "$ref": "csm-schema.json"
                },
                {
                    "required": ["version", "deploymentDescriptors"],
                    "properties": {
                        "version": {
                            "type": "string",
                            "pattern": "^(0|[1-9]\\d*)\\.(0|[1-9]\\d*)\\.(0|[1-9]\\d*)(?:-((?:0|[1-9]\\d*|\\d*[a-zA-Z-][0-9a-zA-Z-]*)(?:\\.(?:0|[1-9]\\d*|\\d*[a-zA-Z-][0-9a-zA-Z-]*))*))?(?:\\+([0-9a-zA-Z-]+(?:\\.[0-9a-zA-Z-]+)*))?$",
                            "description": "The version number of this version (follows semver format)"
                        },
                        "deploymentDescriptors": {
                            "type": "array",
                            "items": {
                                "$ref": "#/definitions/deployment-descriptor"
                            },
                            "minItems": 1,
                            "description": "The list of deployment descriptors that can be used to deploy exactly this version of the service"
                        }
                    }
                }
            ]
        },
        "deployment-descriptor": {
            "oneOf": [
                {
                    "$ref": "#/definitions/deployment-descriptor-helm"
                }
            ]
        },
        "deployment-descriptor-helm": {
            "type": "object",
            "required": ["type", "chart"],
            "properties": {
                "type": {
                    "const": "helm",
                    "description": "Identifier that specifies the type of deployment descriptor"
                },
                "chart": {
                    "type": "string",
                    "format": "uri-reference",
                    "description": "URI to the Helm chart"
                }
            }
        }
    }
}
